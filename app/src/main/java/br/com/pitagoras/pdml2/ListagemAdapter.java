package br.com.pitagoras.pdml2;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ListagemAdapter extends RecyclerView.Adapter<ListagemAdapter.ListagemViewHolder> {


    private List<String> nomes;

    public ListagemAdapter(List<String> nomes) {
     this.nomes = nomes;
    }

    @NonNull
    @Override
    public ListagemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.listagem_nomes, viewGroup, false);
        return new ListagemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ListagemViewHolder holder, int position) {
        // AÇÃO DE SETAR NOME NA LISTA
        holder.nome.setText(this.nomes.get(position));
    }

    @Override
    public int getItemCount() {
        return this.nomes == null ? 0 : this.nomes.size();
    }

    public static class ListagemViewHolder extends RecyclerView.ViewHolder {

        TextView nome;

        public ListagemViewHolder(View v) {
            super(v);
            nome = v.findViewById(R.id.tvValorNome);
        }

    }

}